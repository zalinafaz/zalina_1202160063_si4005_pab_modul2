package com.example.android.zalina_1202160063_si4005_pab_modul2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Summary extends AppCompatActivity {
    private TextView tujuan, berangkat, waktu, pulang, harga, jml_tiket;
    private Button konfirmasi;
    private  TextView Tharga;

    private static final String LOG_TAG = Summary.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        Log.d(LOG_TAG, "-------");
        Log.d(LOG_TAG, "onCreate");

        tujuan = findViewById(R.id.tampil_kota);
        berangkat = findViewById(R.id.tampil_tglberangkat);
        jml_tiket = findViewById(R.id.jumlah_tiket);
        pulang = findViewById(R.id.tgl_pulang);
        Tharga = findViewById(R.id.tampil_harga);
        konfirmasi = findViewById(R.id.btn_konfirmasi);

        String kota = getIntent().getStringExtra("KOTA_TUJUAN");
        String tgl = getIntent().getStringExtra("TGL_BERANGKAT");
        String wkt = getIntent().getStringExtra("WAKTU_BERANGKAT");
        String wkt_pulang = getIntent().getStringExtra("WAKTU_PULANG");
        String tiket = getIntent().getStringExtra("JML_TIKET");
        String tanggal_pulang = getIntent().getStringExtra("TGL_PULANG");
        String hargaa = getIntent().getStringExtra("HARGA_TIKET");

        tujuan.setText(kota);
        berangkat.setText(tgl + " " + wkt);
        jml_tiket.setText(tiket);
        pulang.setText(tanggal_pulang + " " + wkt_pulang);
        Tharga.setText(hargaa);

        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Toast.makeText(getApplicationContext(), "Terima Kasih !!.. Selamat Sampai Tujuan", Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }
}