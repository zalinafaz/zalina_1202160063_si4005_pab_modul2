package com.example.android.zalina_1202160063_si4005_pab_modul2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    int saldoLama;
    private Button beliTiket;
    private Switch PulangPergi;

    private Spinner kotaTujuan;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TextView tvDateResult;
    private TextView tvDateResultPulang;
    private TextView btDatePicker;

    private TextView tvTimeResult,tvTimeResultPulang, waktu, jml_tiket, hrgTiket;
    private TimePickerDialog timePickerDialog;

    //message dialog
    private TextView inputDialog;
    private TextView Saldo;
    private TextView pulang;
    private TextView waktuPulang;

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(LOG_TAG, "-------");
        Log.d(LOG_TAG, "onCreate");

        Switch onOffSwitch = (Switch) findViewById(R.id.switch1);
        pulang = findViewById(R.id.tglpulang);
        waktuPulang = findViewById(R.id.waktupulang);
        final TextView tglpulang = findViewById(R.id.tampil_tglplg);

        beliTiket = findViewById(R.id.btn_beli);
        jml_tiket = findViewById(R.id.nilaijumlahtiket);
        kotaTujuan  = findViewById(R.id.tampilKota);



        inputDialog = findViewById(R.id.topup);
        Saldo = findViewById(R.id.saldoasli);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        tvDateResult = findViewById(R.id.tampil_tglbgkt);
        tvDateResultPulang = findViewById(R.id.tampil_tglplg);
        tvTimeResultPulang = findViewById(R.id.tampil_waktuplg);
        btDatePicker = findViewById(R.id.tglberangkat);

        tglpulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialogPulang();
            }
        });

        waktuPulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialogPulang();
            }
        });
        tvTimeResult = findViewById(R.id.tampil_waktubgkt);
        waktu = findViewById(R.id.waktuberangkat);

        beliTiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int jumlah_tiket = Integer.parseInt(jml_tiket.getText().toString());
                Log.d("testt", "klikkkkkk: ");
                int hargaTiket;
                int getHarga = kotaTujuan.getSelectedItemPosition();
                Log.d("harga", String.valueOf(getHarga));
                String get_saldo = Saldo.getText().toString();
                int iniSaldo = Integer.parseInt(get_saldo);

//
                if (getHarga == 0) {
                    hargaTiket = 85000;
                } else if (getHarga == 1) {
                    hargaTiket = 150000;
                } else {
                    hargaTiket = 70000;
                }

                String tiket = jml_tiket.getText().toString();
                int jumlah = Integer.parseInt(tiket);
                int totalHarga = jumlah * hargaTiket;
                int totalSaldo = iniSaldo - totalHarga;
                String hargaiketku = String.valueOf(totalHarga);

                if (iniSaldo >= totalHarga) {
                    Saldo.setText(String.valueOf(totalSaldo));
                    Intent intent = new Intent(MainActivity.this, Summary.class);
                    intent.putExtra("KOTA_TUJUAN", kotaTujuan.getSelectedItem().toString());
                    intent.putExtra("TGL_BERANGKAT", tvDateResult.getText().toString());
                    intent.putExtra("WAKTU_BERANGKAT", tvTimeResult.getText().toString());
                    intent.putExtra("WAKTU_PULANG", tvTimeResultPulang.getText().toString());
                    intent.putExtra("JML_TIKET", jml_tiket.getText().toString());
                    intent.putExtra("TGL_PULANG", tglpulang.getText().toString());
                    intent.putExtra("HARGA_TIKET", hargaiketku);//

                    startActivity(intent);

                } else {
                    Toast.makeText(getApplicationContext(), "Maaf saldo Anda kurang", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        addListenerOnSpinnerItemSelection();

        inputDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog();
            }
        });

        waktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });

        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        pulang.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                showDateDialogPulang();
            }
        });


        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v("Switch State=", "" + isChecked);

                if (isChecked) {
                    pulang.setVisibility(View.VISIBLE);
                    tglpulang.setVisibility(View.VISIBLE);
                    waktuPulang.setVisibility(View.VISIBLE);
                } else {
                    tglpulang.setVisibility(View.GONE);
                    pulang.setVisibility(View.GONE);
                    waktuPulang.setVisibility(View.GONE);
                }
            }
        });
    }

    public void addListenerOnSpinnerItemSelection() {
//        kotaTujuan = findViewById(R.id.tampilKota);
        kotaTujuan.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    private void showInputDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.popup_input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = promptView.findViewById(R.id.editText);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Tambah Saldo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String saldoAwal = Saldo.getText().toString();
                        String jumlah = editText.getText().toString();

                        saldoLama = Integer.parseInt(saldoAwal);
                        int jumlahSaldo = Integer.parseInt(jumlah);

                        int totalSaldo = saldoLama + jumlahSaldo;
                        Saldo.setText(String.valueOf(totalSaldo));
                        Toast.makeText(getApplicationContext(), "Top up berhasil", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showDateDialog() {
        Calendar newCalender = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);

                tvDateResult.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalender.get(Calendar.YEAR), newCalender.get(Calendar.MONTH), newCalender.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void showDateDialogPulang() {
        Calendar newCalender = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);

                tvDateResultPulang.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalender.get(Calendar.YEAR), newCalender.get(Calendar.MONTH), newCalender.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    private void showTimeDialog() {
        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                tvTimeResult.setText(hourOfDay + ":" + minute);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();

    }



    private void showTimeDialogPulang() {
        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                tvTimeResultPulang.setText(hourOfDay + ":" + minute);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        timePickerDialog.show();

    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }
}